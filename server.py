from bottle import static_file, run

from api import *


@get("/")
def home():
    return static("index.html")


@get('/<path:path>')
def static(path):
    return static_file(path, "webapp")


run(host='0.0.0.0', port=port, server='cherrypy')
