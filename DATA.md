# Data Loading
The core data of SFVSim is all contained in one single file: `StreetFighterV.zip`.

## sfvfile
Starting June 9th 2018, sfvsim uses the [sfvfile project](https://gitlab.com/loic.petit/sfvfile).

## Just give me the file
If you don't want to build your own export, I host versions of it [on my server](https://petitl.fr/sfv/)

## Loading data from the game
As you may know, SFV data is located inside Pak files in `Content\Paks\` inside Steam `StreetFighterV` directory.

To extract the paks and convert the uassets files, you can download the [extraction toolkit](https://petitl.fr/sfv/extract-moves.zip)
I prepared. It is based on [quickbms](http://aluigi.altervista.org/quickbms.htm).
  
Once you have downloaded the zip, extract everything in the `Content\Paks` directory and execute `extract-moves.bat`.
This will create a `moves` directory with all the `uassets`. Then go inside the moves directory and create a zip file
using the StreetFighterV directory.
