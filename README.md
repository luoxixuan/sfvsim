# SFV Sim
SFV Sim is a simulator which tries to mimic the behaviour of Street Fighter V.

Street Fighter V base its engine using _scripts_ that dictates the positions of the characters, the hitboxes, the hit 
 effects... This project interprets those scripts and executes situations.

## Is it accurate?
You'd be surprise on how much it actually works! The project is tested against data extracted from the game. You can
consult the list of tests in the `tests` directory. I try to take into account up to the floating point errors.

Be aware that the SFV engine contains a lot of elements and subtleties and even bugs. But even that, I try to handle it.

## Is it fast?
Of course this will depend on your computer and how fast your CPU is. But as a reference, a ~250 frames situation like 
Cammy's V-Arrow + V-Spike juggle takes 60ms on my laptop (i.e. ~4 frames). I tried to make the runner fast. The only way 
to massively improve the performances would now need to rewrite everything in a native language. 

## Webapp
The simulator is a python based project, yet a webapp can be consulted to test situations.

Access to a live version [here](https://petitl.fr/sfvsim/)

The webapp is limited to 1000 frames situations to avoid overloading my servers.

## Install it
First and foremost, you need a file called `StreetFighterV.zip`, please consult [how to get it / build it](DATA.md).

Then you need `python 3.5+` with `pip` and `gzip` available (standard distribution usually covers this).

Install [sfvfile](https://gitlab.com/loic.petit/sfvfile) (see instructions).

Install dependencies
```bash
pip install -r deps.txt
```

Launch the server:
```bash
python server.py versions/sx.xxx.txt 8765
```

You can then consult the webapp in your browser at http://localhost:8765

## Known issues and bug report
You can consult all the confirmed issues [here](https://gitlab.com/loic.petit/sfv-simulator/issues).

However here's a list of known unsupported major behaviours:
* Nadeshiko (it's considered as a third character, not handled at all right now)
* Camera walls (no camera tracking is done yet, it makes R.Mika's rope fail or Claw's Barcelona reach the corner)

The rest is considered as bugs.

To submit a new report, please use the webapp which contains a *Report* button. This will create a confidential issue 
with reproducible step for me, if bug is confirmed I'll create a real ticket and notify you when it's fixed.

Of course you can always submit merge requests. Do not hesitate to contact me if you are interested by the project in
any way.

## Author
Loïc _WydD_ Petit [@WydD](https://twitter.com/WydD) [/u/-WydD-](https://reddit.com/u/-WydD-)

## Licence
The project code is public and under the [MIT Licence](LICENSE) 

## Thanks
This project was made possible using the help of:
* lullius (without MoveTool nothing would be possible)
* ToolAssisted (DIFF TOOL TOO STRONG LOL)
* dantarion (BoxDox of course and other advices)
* Killbox and the MysteriousMod project (a lot of script knowledge)
* Hatson (help with some game mechanics comprehension)

All the alpha and beta testers.
