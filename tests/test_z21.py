from tests.util import SimulationRun


def test_z21_5hk_cc():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-5hk-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HK")


def test_z21_5hp_cc():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-5hp-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HP")


def test_z21_hyakki_gojin():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-hyakki-gojin", 30)
    run.execute("STAND", 2)
    run.execute("HYAKKISHU_M", 26)
    run.execute_and_check("HYAKKI_GOJIN")


def test_z21_hyakki_gojin_meaty():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-hyakki-gojin-meaty", 18)
    run.execute("STAND", 2)
    run.execute("HYAKKISHU_M", 25)
    run.execute_and_check("HYAKKI_GOJIN")


def test_z21_hyakki_gojin_meaty_guard():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-hyakki-gojin-meaty-guard", 18)
    run.p2.hit_effect = "GUARD"
    run.execute("STAND", 2)
    run.execute("HYAKKISHU_M", 25)
    run.execute_and_check("HYAKKI_GOJIN")


def test_z21_gnd_tatsu_lk():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-gnd-tatsu-lk", 40)
    run.execute("STAND", 2)
    run.execute_and_check("GND_TATSUMAKI_L")


def test_z21_gnd_tatsu_mk():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-gnd-tatsu-mk", 40)
    run.execute("STAND", 2)
    run.execute_and_check("GND_TATSUMAKI_M")


def test_z21_gnd_tatsu_hk():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-gnd-tatsu-hk", 30)
    run.execute("STAND", 2)
    run.execute_and_check("GND_TATSUMAKI_H")


def test_z21_gnd_tatsu_ex():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-gnd-tatsu-ex", 30)
    run.execute("STAND", 2)
    run.execute_and_check("GND_TATSUMAKI_EX")


def test_z21_gnd_tatsu_ex_guard():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-gnd-tatsu-ex-guard", 30)
    run.p2.hit_effect = "GUARD"
    run.execute("STAND", 2)
    run.execute_and_check("GND_TATSUMAKI_EX")


def test_z21_air_tatsu():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-air-tatsu")
    run.execute("JUMP_F", 33)
    run.execute_and_check("AIR_TATSUMAKI_L")


def test_z21_air_zanku():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-jump-f-zanku-l")
    run.execute("JUMP_F", 33)
    run.execute_and_check("ZANKU_L_FD")


def test_z21_shoryu_m():
    run = SimulationRun("2.016", "Z21", "RYU", "z21-shoryu-m", 40)
    run.execute("STAND", 1)
    run.execute_and_check("SHOURYU_M")


def test_z21_shoryu_h():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-shoryu-h", 40)
    run.execute("STAND", 1)
    run.execute_and_check("SHOURYU_H")


def test_z21_vskill_parry():
    run = SimulationRun("2.021", "Z21", "CMY", "z21-vskill-parry", 40)
    run.execute_and_check("V_SKILL_UP", script_p2="5MP")


def test_z21_vt_shoryu_l():
    run = SimulationRun("2.021", "Z21", "CMY", "z21-vt-shoryu-l", 40)
    run.p1.vtrigger = 3000
    run.execute("STAND", 1)
    run.execute_and_check("VT_SHOURYU_L")


def test_z21_vt_shoryu_h_whiff():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-vt-shoryu-h-whiff")
    run.p1.vtrigger = 3000
    run.execute("STAND", 2)
    run.execute_and_check("VT_SHOURYU_H")


def test_z21_vt_shoryu_h_aa():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-vt-shoryu-h-aa")
    run.p1.vtrigger = 3000
    run.execute("STAND", 22, "JUMP_F")
    run.execute_and_check("VT_SHOURYU_H")


def test_z21_gnd_tatsu_lk_2hk_juggle():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-tatsu-l-2hk-juggle", 40)
    run.execute("STAND", 2)
    run.execute_and_check(["GND_TATSUMAKI_L", "2HK"])


def test_z21_gnd_tatsu_lk_2hk_whiff():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-tatsu-l-2hk-whiff", 40)
    run.execute("STAND", 2)
    run.execute("GND_TATSUMAKI_L", 46)
    run.execute("STAND", 4)
    run.execute_and_check("2HK")


def test_z21_gnd_tatsu_lk_5hp_otg():
    run = SimulationRun("2.021", "Z21", "RYU", "z21-tatsu-l-5hp-otg", 40)
    run.execute("STAND", 2)
    run.execute_and_check(["GND_TATSUMAKI_L", "6HP"])

