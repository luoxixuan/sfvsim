from tests.util import SimulationRun


def test_jri_throw_n():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-throw-n", 40)
    run.execute_and_check("THROW_N")


def test_jri_2hp_cc():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-2hp-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("2HP")


def test_jri_5hk_cc():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-5hk-cc", 40)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("5HK")


def test_jri_v_skill_lv3():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-v-skill-lv3")
    run.p1.hold = 120
    run.execute_and_check("V_SKILL")


def test_jri_v_skill_lv1():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-v-skill-lv1")
    run.execute_and_check("V_SKILL")


def test_jri_tensen_l_guard_crouched():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-tensen-l-guard-crouched", 40)
    run.p2.hit_effect = "GUARD"
    run.execute("STAND")
    run.execute_and_check("TENSEN_L", script_p2="CROUCH")


def test_jri_tensen_l_guard_stand():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-tensen-l-guard-stand", 40)
    run.p2.hit_effect = "GUARD"
    run.execute("STAND")
    run.execute_and_check("TENSEN_L")


def test_jri_tensen_l_hit():
    run = SimulationRun("2.021", "JRI", "RYU", "jri-tensen-l-hit", 40)
    run.execute("STAND")
    run.execute_and_check("TENSEN_L")
