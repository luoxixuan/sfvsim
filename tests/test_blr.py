from tests.util import SimulationRun


def test_blr_whiff_barcelona():
    run = SimulationRun("2.021", "BLR", "RYU", "blr-whiff-barcelona")
    run.execute("STAND", 2)
    run.execute_and_check("6_BARCELONA_L")


def test_blr_whiff_barcelona_corner():
    run = SimulationRun("2.021", "BLR", "RYU", "blr-whiff-barcelona-corner", x_p1=5, x_p2=7.5)
    run.execute("STAND", 2)
    run.execute_and_check("6_BARCELONA_L")


def test_blr_whiff_barcelona_mk_corner():
    run = SimulationRun("2.021", "BLR", "RYU", "blr-whiff-barcelona-mk-corner", x_p1=5, x_p2=7.5)
    run.execute("STAND", 2)
    run.execute_and_check("6_BARCELONA_M")


def test_blr_izuna_l_corner():
    run = SimulationRun("2.021", "BLR", "RYU", "blr-izula-l-corner", 40, x_p1=5, x_p2=7.5)
    run.execute("STAND", 1)
    run.execute_and_check("G_IZUNA_L")