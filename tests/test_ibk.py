from tests.util import SimulationRun


def test_ibk_6hk_whiff():
    run = SimulationRun("2.021", "IBK", "RYU", "ibk-6hk-whiff")
    run.execute_and_check("BONSHO(6HK)")


def test_ibk_5mk():
    run = SimulationRun("2.021", "IBK", "RYU", "ibk-5mk", 40)
    run.execute_and_check("5MK")


def test_ibk_5mp():
    run = SimulationRun("2.021", "IBK", "RYU", "ibk-5mp", 40)
    run.execute_and_check("5MP")


def test_ibk_5mp_5mk_link():
    run = SimulationRun("2.021", "IBK", "RYU", "ibk-5mk-5mp", 40)
    run.execute_and_check(["5MK", "5MP"])


def test_ibk_6hk_hit():
    run = SimulationRun("2.021", "IBK", "RYU", "ibk-6hk-hit", 30)
    run.execute_and_check("BONSHO(6HK)")


def test_ibk_6hk_hit_cc():
    run = SimulationRun("2.021", "IBK", "RYU", "ibk-6hk-hit-cc", 30)
    run.p2.hit_effect = "COUNTERHIT"
    run.execute_and_check("BONSHO(6HK)")


def test_ibk_kunai_ex():
    run = SimulationRun("2.021", "IBK", "RYU", "ibk-kunai-ex-lh")
    run.execute("STAND", 2)
    run.execute_and_check("KUNAI_EX_LH")
