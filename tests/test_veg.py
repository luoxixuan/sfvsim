from tests.util import SimulationRun


def test_veg_headpress_l():
    run = SimulationRun("2.021", "VEG", "RYU", "veg-headpress-l")
    run.execute_and_check("HEADPRESS_L")


def test_veg_devilreverse_h():
    run = SimulationRun("2.021", "VEG", "RYU", "veg-devilreverse-h")
    run.execute("HEADPRESS_H", 14)
    run.execute_and_check("DEVILREVERSE_H")

