import json
import os

from model import PlayerState
from simulation.move_queue import MoveQueue, execute_simulation, MoveQueueItem
from simulation.simulator import Simulator, position_logger, transition_logger

version_alias = {
    "2.016": "s2.016-kolin-update",
    "2.021": "s2.021-ed-update",
    "2.080": "s2.080-zeku-patch",
    "3.060": "s3.060-g-sagat-update"
}
sim_cache = dict()


def get_sim(version):
    if version in sim_cache:
        return sim_cache[version]
    sim = Simulator("versions/" + version_alias[version] + ".txt")
    sim_cache[version] = sim
    return sim


def assert_float_eq(d1: float, d2: float, frame: int, s: str):
    # noinspection PyTypeChecker
    result = -0.00002 < d1 - d2 < 0.00002
    if not result:
        position_logger.warn("[%s] %f SHOULD BE %f on frame %d (%f)" % (s, d1, d2, frame, (d1 - d2)))
        return 1
    return 0


def assert_position(p1, p2, position, frame):
    if len(position) > 2:
        # measure contains scripts
        if p1.script.script != position[2] and (p1.script.script > 3 or position[2] > 3):
            transition_logger.warn("[p1] Script %d should actually be %d" % (p1.script.script % 2000, position[2]))
        if p2.script.script != position[3] and (p2.script.script > 3 or position[3] > 3):
            transition_logger.warn("[p2] Script %d should actually be %d" % (p2.script.script % 2000, position[3]))
    return assert_float_eq(p1.pos.coord[0], position[0][0], frame, "p1.x") + \
           assert_float_eq(p1.pos.coord[1], position[0][1], frame, "p1.y") + \
           assert_float_eq(p2.pos.coord[0], position[1][0], frame, "p2.x") + \
           assert_float_eq(p2.pos.coord[1], position[1][1], frame, "p2.y")


def as_move_queue_items(move_name, frames):
    if isinstance(move_name, list):
        return [MoveQueueItem(m, frames) for m in move_name]
    return [MoveQueueItem(move_name or "STAND", frames)]


class SimulationRun:
    """
    @type p1: PlayerState
    @type p2: PlayerState
    @type states: list[tuple[model.PlayerState]]
    """

    def __init__(self, version, p1_char, p2_char, test_file, forward_p1=0, forward_p2=0, x_p1=-1.5, x_p2=1.5):
        test_file = json.load(open(os.path.dirname(__file__) + "/data/%s.json" % test_file))
        self.positions = test_file["positions"]
        self.frame_advance = test_file["frame_advance"]
        self.p1 = PlayerState(p1_char, x_p1, 0)
        self.p2 = PlayerState(p2_char, x_p2, 0, opponent=self.p1)
        self.error_count = 0
        self.states = []
        self.frame = 0
        self.sim = get_sim(version)
        fwd = "FORWARD"
        bwd = "BACKWARD"
        common = min(abs(forward_p1), abs(forward_p2))
        if common > 0:
            self.execute(fwd if forward_p1 > 0 else bwd, common, fwd if forward_p2 > 0 else bwd)
        next_frame_count = max(abs(forward_p1), abs(forward_p2)) - common
        if abs(forward_p1) > common:
            self.execute(fwd if forward_p1 > 0 else bwd, next_frame_count)
        elif abs(forward_p2) > common:
            self.execute(["STAND"], next_frame_count, move_name_p2=fwd if forward_p2 > 0 else bwd)

    def execute(self, move_name_p1, frames=None, move_name_p2=None):
        def collect(p1, p2):
            self.error_count += assert_position(p1, p2, self.positions[self.frame], self.frame)
            self.frame += 1

        self.sim.execute(self.p1, self.p2, move_name_p1, frames, func=collect, move_name_p2=move_name_p2)

        # p1_queue = MoveQueue(as_move_queue_items(move_name_p1, frames), self.p1_rest)
        # p2_queue = MoveQueue(as_move_queue_items(move_name_p2, frames), self.p2_rest)
        # execute_simulation(sim, self.p1, p1_queue, self.p2, p2_queue, collect, max_frames=frames or 1000)

    def execute_and_check(self, script_p1, frames=None, script_p2=None, known_error_count=0):
        self.execute(script_p1, frames, script_p2)
        # Check that the rest of the recording are according to plan
        for f in range(self.frame, len(self.positions)):
            self.error_count += assert_position(self.p1, self.p2, self.positions[f], self.frame)
        # Check if our frame advance is the same as measured
        if self.frame_advance != self.p2.recovery:
            position_logger.warn("Frame advance: expected %d, got %d" % (self.frame_advance, self.p2.recovery))
        assert self.frame_advance == self.p2.recovery
        # Now check the positions
        if self.error_count != known_error_count:
            position_logger.warn("Total error count: expected %d, got %d" % (known_error_count, self.error_count))
        assert self.error_count == known_error_count
