from tests.util import SimulationRun


def test_rmk_brimstone_l():
    run = SimulationRun("2.021", "RMK", "RYU", "rmk-brimstone-l", 45)
    run.execute("CROUCH", 1)
    run.execute_and_check("BRIMSTONE_L")


def test_rmk_brimstone_whiff():
    run = SimulationRun("2.021", "RMK", "RYU", "rmk-brimstone-whiff", 1)
    run.execute("CROUCH", 1)
    run.execute_and_check("BRIMSTONE_L")


def test_rmk_2mp_crouched():
    run = SimulationRun("2.021", "RMK", "RYU", "rmk-2mp-crouched", 50)
    run.execute_and_check("2MP", script_p2="CROUCH")


def test_rmk_throw_crouched():
    run = SimulationRun("2.021", "RMK", "RYU", "rmk-throw-crouched", 50)
    run.execute_and_check("THROW_F", script_p2="CROUCH")


def test_rmk_throw_f():
    run = SimulationRun("2.021", "RMK", "RYU", "rmk-throw-f", 50)
    run.execute_and_check("THROW_F")


def test_rmk_throw_b():
    run = SimulationRun("2.021", "RMK", "RYU", "rmk-throw-b", 50)
    # Broken due to the wall
    run.execute_and_check("THROW_B", known_error_count=106)

