from tests.util import SimulationRun


def test_rsd_raw_vt():
    run = SimulationRun("2.021", "RSD", "RYU", "rsd-raw-vt")
    run.execute_and_check("VT_IUSAL", 300)

# way too broken but the idea is there
# def test_rsd_vt_mixer_l():
#     run = SimulationRun("2.021", "RSD", "RYU", "rsd-vt-mixer-l")
#     run.execute_and_check(["VT_IUSAL", "SPININGMIXER_L"])

