from tests.util import SimulationRun


def test_dsm_teleport_ppp():
    run = SimulationRun("2.021", "DSM", "RYU", "dsm-teleport-ppp", 1)
    run.execute("STAND")
    run.execute_and_check("TELEPO-623PPP")


def test_dsm_yoga_flame_l():
    run = SimulationRun("2.021", "DSM", "RYU", "dsm-yoga-flame-l", 40)
    run.execute("STAND")
    run.execute_and_check("YOGAFLAME_L")
