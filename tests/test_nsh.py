from tests.util import SimulationRun


def test_nsh_moonsault_ex():
    run = SimulationRun("3.060", "NSH", "RYU", "nsh-moonsault-ex")
    run.execute("STAND", 2)
    run.execute_and_check("MOONSULT_EX")


def test_nsh_vtrigger_1():
    run = SimulationRun("3.060", "NSH", "RYU", "nsh-vtrigger-1")
    run.execute_and_check("VT1_SONICMOVE_6")
