from tests.util import SimulationRun


def test_walk():
    sim = SimulationRun("2.021", "CMY", "RYU", "walk", 120, 120)
    assert sim.error_count == 0


def test_walk_corner():
    sim = SimulationRun("2.021", "CMY", "RYU", "walk-corner", 20, x_p1=7, x_p2=7.5)
    assert sim.error_count == 0